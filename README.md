# jsBanner

A simple banner ASCII generator do be used with javascript console.

I was working in a WebRTC application these days and one of the computers was some far away from me. Each time some signal need to be checked I need to stand-up and walk for a meter and a half, more or less, returning quickly to my chair to se the console of the other points.

So I develop this.

## Example:

>  console.log(bannerObj("Singal 1"));

It will print a pretty ASCII big banner

```
███████   ██     █████  ███  ██  █████  ██                 ██   
██        ██    ██      ████ ██ ██   ██ ██                ███   
███████   ██    ██  ███ ██ ████ ███████ ██                 ██   
     ██   ██    ██   ██ ██  ███ ██   ██ ██                 ██   
███████   ██     █████  ██   ██ ██   ██ ███████            ██   
```

So, now I don't need even to walk and return running to my chair.